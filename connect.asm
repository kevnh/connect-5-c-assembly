.globl main

.text

main:
	lw $s2, size		# size = 54
	li $s3, 4		# index multiplier (convenience)

	jal printIntro		# printIntro()
	move $a1, $v1		# arg1 = order

	la $a0, board		# arg0 = board (address; also constant)
	jal setupBoard		# setupBoard()
	jal gameLoop		# gameLoop(board)

	li $v0, 10		# syscode 10 (exit)
	syscall

printIntro:
	move $t9, $ra		# stack_temp = $ra
	jal push		# push()

	li $v0, 4		# syscode 4 (print string)
	la $a0, intro_p1	# arg0 = intro part 1
	syscall

	li $v0, 1		# syscode 1 (print int)
	li $a0, 1		# arg0 = 1
	syscall

	li $v0, 4		# syscode 4 (print string)
	la $a0, period		# arg0 = "."
	syscall

	li $v0, 1		# syscode 1 (print int)
	li $a0, 0		# arg0 = 0
	syscall

	li $v0, 4		# syscode 4 (print string)
	la $a0, newline		# arg0 = "\n"
	syscall
	
	la $a0, intro_p2	# arg0 = intro_p2
	syscall

	la $a0, number		# arg0 = "\nNumber "
	syscall

	li $v0, 1		# syscode 1 (print int)
	li $a0, 1		# arg0 = 1
	syscall

	li $v0, 4		# syscode 4 (print string)
	la $a0, colon		# arg0 = ": "
	syscall

	li $v0, 5		# syscode 5 (read number)
	syscall
	sw $v0, m_w		# m_w = input
	move $s0, $v0		# $s0 = m_w

	li $v0, 4		# syscode 4 (print string)
	la $a0, number		# arg0 = "\nNumber "
	syscall

	li $v0, 1		# syscode 1 (print int)
	li $a0, 2		# arg0 = 2
	syscall

	li $v0, 4		# syscode 4 (print string)
	la $a0, colon		# arg0 = ": "
	syscall

	li $v0, 5		# syscode 5 (read int)
	syscall
	sw $v0, m_z		# m_z = input
	move $s1, $v0		# $s1 = m_z

	li $v0, 4		# syscode 4 (print string)
	la $a0, intro_p3	# arg0 = intro_p3
	syscall

	li $a0, 0			# arg0 = 0
	li $a1, 1			# arg1 = 1
	jal get_random_in_range		# get_random_in_range(0, 1)
	move $t0, $v1			# temp1 = get_random_in_range(0, 1)

	printI_if1:
		li $t1, 1			# temp2 = 1
		bne $t0, $t1, printI_else1	# if temp1 != 1
		la $a0, human			# arg0 = "HUMAN"
		j printI_endif_el1		# end ifelse
	printI_else1:
		la $a0, computer		# arg0 = "COMPUTER"
	printI_endif_el1:

	syscall

	la $a0, intro_p4	# arg0 = intro_p4
	syscall

	jal pop			# stack_temp = return address
	jr $t9

setupBoard:
	li $t0, 67		# letter = 67
	li $t1, 0		# i = 0
	setupB_for1:
		bge $t1, $s2, setupB_endfor1	# if i >= size
		mul $t5, $t1, $s3		# temp4 = i * 4
		add $t4, $t5, $a0		# temp3 = &board[i]
		
		setupB_if1:
			li $t2, 9			# temp1 = 9
			div $t1, $t2			# i / 9
			mfhi $t2			# temp1 = i % 9
			bne $t2, $zero, setupB_if1_1	# if temp1 != 0
			
			sw $t0, 0($t4)		# board[i] = letter
			j setupB_endif_el1	# end ifelse
		setupB_if1_1:
			li $t3, 8			# temp2 = 8
			bne $t2, $t3, setupB_else1	# if temp1 != 8
			
			sw $t0, 0($t4)			# board[i] = letter

			setupB_if2:
				li $t2, 67		# temp1 = 67
				bne $t0, $t2, setupB_else2	# if letter != 67

				li $t0, 72		# letter = 72
				j setupB_endif_el2
			setupB_else2:
				li $t0, 67		# letter = 67
			setupB_endif_el2:

			j setupB_endif_el1		# end ifelse
		setupB_else1:
			li $t3, 46			# temp2 = 46
			sw $t3, 0($t4)			# board[i] = 46
		setupB_endif_el1:
		
		addi $t1, $t1, 1			# i++
		j setupB_for1

	setupB_endfor1:
	jr $ra
	
gameLoop:
	move $t9, $ra		# stack_temp = return address
	jal push		# push stack_temp

	li $t0, 0		# finished = 0
	move $t1, $a1		# temp1 = order
	
	gameL_while1:
		bne $t0, $zero, gameL_endwhile1		# if finished != 0
		jal printBoard				# printBoard(board)
		
		gameL_if1:
			li $t2, 1			# temp2 = 1
			bne $t1, $t2, gameL_else1	# if temp1 != 1

			jal playerTurn		# playerTurn(board)
			jal checkWin		# checkWin(board)
			move $t0, $v1		# finished = checkWin(board)

			bne $t0, $zero, gameL_endwhile1	# if finished != 0

			jal computerTurn	# computerTurn(board)
			j gameL_if_el1		# end ifelse
		gameL_else1:
			jal computerTurn	# computerTurn(board)
			jal checkWin		# checkWin(board)
			move $t0, $v1		# finished = checkWin(board)

			bne $t0, $zero, gameL_endwhile1	# if finished != 0

			jal playerTurn		# playerTurn(board)
		gameL_if_el1:
		
		jal checkWin		# checkWin(board)
		move $t0, $v1		# finished = checkWin(board)

		j gameL_while1
	gameL_endwhile1:
	
	jal printBoard				# printBoard(board)

	li $v0, 4				# syscode 4 (print string)
	gameL_if2:
		li $t2, 72			# temp2 = 72
		bne $t0, $t2, gameL_if2_1	# if finished != 72
		
		la $a0, win			# arg0 = win

		j gameL_if_el2			# end ifelse
	gameL_if2_1:
		li $t2, 67			# temp2 = 67
		bne $t0, $t2, gameL_else2	# if finished != 67

		la $a0, lose			# arg0 = lose

		j gameL_if_el2			# end ifelse
	gameL_else2:
		la $a0, tie			# arg0 = tie
	gameL_if_el2:
	syscall

	jal pop			# stack_temp = return address
	jr $t9
	
checkWin:
	move $t9, $ra		# stack_temp = return address
	jal push		# push stack_temp
	
	li $t2, 0		# i = 0
	
	checkW_for1:
		move $a1, $t2			# arg1 = i
		bge $t2, $s2, checkW_endfor1	# if i >= size

		mul $t3, $t2, $s3	# temp1 = i * 4
		add $t3, $t3, $a0	# temp1 = temp1 + board
		lw $t4, 0($t3)		# temp2 = board[i]
	
		li $t5, 46		# temp3 = 46
		checkW_if1:
			bne $t4, $t5, checkW_if1_1	# if board[i] != 46
			j checkW_if_el1
		checkW_if1_1:
			li $t5, 17			# temp3 = 17
			ble $t2, $t5, checkW_else1	# if i <= 17
			
			addi $t5, $t3, 4	# temp3 = temp1 + 4
			lw $t5, 0($t5)		# temp3 = board[i+1]

			checkW_if2:
				bne $t4, $t5, checkW_if_el2	# if board[i] != board[i+1]
				jal checkHorizontal		# checkHorizontal(board, i)
				move $t9, $v1			# temp9 = checkHorizontal(...)
				move $v1, $t4			# return val = board[i]
				bne $t9, $zero, checkW_return	# if checkHorizontal(...) != 0
			checkW_if_el2:

		checkW_else1:
			addi $t5, $t3, 36	# temp3 = temp2 + 36
			lw $t5, 0($t5)		# temp3 = board[i+9]

			checkW_if3:
				bne $t4, $t5, checkW_if_el3	# if board[i] != board[i+9]
				jal checkVertical		# checkVertical(board, i)
				move $t9, $v1			# temp9 = checkVertical(...)
				move $v1, $t4			# return val = board[i]
				bne $t9, $zero, checkW_return	# if checkVertical(...) != 0
			checkW_if_el3:

			li $t5, 9		# temp3 = 9
			div $t2, $t5		# i / 9
			mfhi $t5		# temp3 = i % 9
			li $t6, 4		# temp4 = 4

			checkW_if4:
				bgt $t5, $t6, checkW_if_el4	# if i % 9 > 4
				
				addi $t5, $t3, 4	# temp3 = temp2 + 4
				lw $t5, 0($t5)		# temp3 = board[i+1]
				
				checkW_if5:
					bne $t4, $t5, checkW_if_el5	# if board[i] != board[i+1]
					jal checkHorizontal		# checkHorizontal(board, i)
					move $t9, $v1			# temp9 = checkHorizontal(...)
					move $v1, $t4			# return val = board[i]
					bne $t9, $zero, checkW_return	# if checkHorizontal(...) != 0
				checkW_if_el5:

				addi $t5, $t3, 40	# temp3 = temp2 + 40
				lw $t5, 0($t5)		# temp3 = board[i+10]

				checkW_if6:
					bne $t4, $t5, checkW_if_el6	# if board[i] != board[i+10]
					jal checkDiagonalRight		# checkDiagonalRight(board, i)
					move $t9, $v1			# temp9 = checkDiagonalRight(...)
					move $v1, $t4			# return val = board[i]
					bne $t9, $zero, checkW_return	# if checkDiagonalRight(...) != 0
				checkW_if_el6:

			checkW_if_el4:

			li $t5, 9		# temp3 = 9
			div $t2, $t5		# i / 9
			mfhi $t5		# temp3 = i % 9
			li $t6, 4		# temp4 = 4

			checkW_if7:
				blt $t5, $t6, checkW_if_el7	# if i % 9 < 4
				
				addi $t5, $t3, 32	# temp3 = temp2 + 32
				lw $t5, 0($t5)		# temp3 = board[i+8]

				checkW_if8:
					bne $t4, $t5, checkW_if_el8	# if board[i] != board[i+8]
					jal checkDiagonalLeft		# checkDiagonalLeft(board, i)
					move $t9, $v1			# temp9 = checkDiagonalLeft(...)
					move $v1, $t4			# return val = board[i]
					bne $t9, $zero, checkW_return	# if checkDiagonalLeft(...) != 0
				checkW_if_el8:

			checkW_if_el7:

		checkW_if_el1:
	
		addi $t2, $t2, 1	# i++
		j checkW_for1

	checkW_endfor1:

	jal checkTie		# checkTie(board)
	
	checkW_if9:
		beq $v1, $zero, checkW_if_el9	# if checkTie(...) == 0
		li $v1, 2			# return val = 2
		j checkW_return			# return
	checkW_if_el9:

	li $v1, 0		# return val = 0
	checkW_return:
		jal pop		# stack_temp = return address
		jr $t9

checkTie:
	li $t5, 1		# i = 1
	li $t6, 8		# temp1 = 8

	checkT_for1:
		bge $t5, $t6, checkT_endfor1	# if i >= 8
		
		mul $t7, $t5, $s3	# temp2 = i * 4
		add $t7, $t7, $a0	# temp2 = temp2 + board
		lw $t7, 0($t7)		# temp2 = board[i]
		li $t8, 46		# temp2 = 46

		checkT_if1:
			bne $t7, $t8, checkT_if_el1	# if board[i] != 46
			li $v1, 0			# return val = 0
			j checkT_return			# return
		checkT_if_el1:

		addi $t5, $t5, 1	# i++
		j checkT_for1
	checkT_endfor1:

	li $v1, 1		# return val = 1
	checkT_return:
		jr $ra

checkHorizontal:
	li $t5, 2		# j = 2
	li $t6, 5		# temp1 = 5

	checkH_for1:
		bge $t5, $t6, checkH_endfor1	# if i >= 5

		mul $t7, $a1, $s3	# temp2 = index * 4
		add $t7, $t7, $a0	# temp2 = temp2 + board
		
		mul $t8, $t5, $s3	# temp3 = j * 4
		add $t8, $t8, $t7	# temp3 = temp3 + temp2

		lw $t7, 0($t7)		# temp2 = board[index]
		lw $t8, 0($t8)		# temp3 = board[index+j]

		checkH_if1:
			beq $t7, $t8, checkH_if_el1	# if board[index] == board[index+j]
			li $v1, 0			# return val = 0
			j checkH_return			# return
		checkH_if_el1:

		addi $t5, $t5, 1	# j++
		j checkH_for1
	checkH_endfor1:

	li $v1, 1		# return val = 1
	checkH_return:
		jr $ra

checkDiagonalRight:
	li $t5, 2		# j = 2
	li $t6, 5		# temp1 = 5

	checkDR_for1:
		bge $t5, $t6, checkDR_endfor1	# if i >= 5

		mul $t7, $a1, $s3	# temp2 = index * 4
		add $t7, $t7, $a0	# temp2 = temp2 + board
	
		li $t8, 40		# temp3 = 40
		mul $t8, $t5, $t8	# temp3 = j * 40
		add $t8, $t8, $t7	# temp3 = temp3 + temp2

		lw $t7, 0($t7)		# temp2 = board[index]
		lw $t8, 0($t8)		# temp3 = board[index+j]

		checkDR_if1:
			beq $t7, $t8, checkDR_if_el1	# if board[index] == board[index+j]
			li $v1, 0			# return val = 0
			j checkDR_return		# return
		checkDR_if_el1:

		addi $t5, $t5, 1	# j++
		j checkDR_for1
	checkDR_endfor1:

	li $v1, 1		# return val = 1
	checkDR_return:
		jr $ra

checkVertical:
	li $t5, 2		# j = 2
	li $t6, 5		# temp1 = 5

	checkV_for1:
		bge $t5, $t6, checkV_endfor1	# if i >= 5

		mul $t7, $a1, $s3	# temp2 = index * 4
		add $t7, $t7, $a0	# temp2 = temp2 + board
	
		li $t8, 36		# temp3 = 36
		mul $t8, $t5, $t8	# temp3 = j * 36
		add $t8, $t8, $t7	# temp3 = temp3 + temp2

		lw $t7, 0($t7)		# temp2 = board[index]
		lw $t8, 0($t8)		# temp3 = board[index+j]

		checkV_if1:
			beq $t7, $t8, checkV_if_el1	# if board[index] == board[index+j]
			li $v1, 0			# return val = 0
			j checkV_return			# return
		checkV_if_el1:

		addi $t5, $t5, 1	# j++
		j checkV_for1
	checkV_endfor1:

	li $v1, 1		# return val = 1
	checkV_return:
		jr $ra

checkDiagonalLeft:
	li $t5, 2		# j = 2
	li $t6, 5		# temp1 = 5

	checkDL_for1:
		bge $t5, $t6, checkDL_endfor1	# if i >= 5

		mul $t7, $a1, $s3	# temp2 = index * 4
		add $t7, $t7, $a0	# temp2 = temp2 + board
	
		li $t8, 32		# temp3 = 32
		mul $t8, $t5, $t8	# temp3 = j * 32
		add $t8, $t8, $t7	# temp3 = temp3 + temp2

		lw $t7, 0($t7)		# temp2 = board[index]
		lw $t8, 0($t8)		# temp3 = board[index+j]

		checkDL_if1:
			beq $t7, $t8, checkDL_if_el1	# if board[index] == board[index+j]
			li $v1, 0			# return val = 0
			j checkDL_return		# return
		checkDL_if_el1:

		addi $t5, $t5, 1	# j++
		j checkDL_for1
	checkDL_endfor1:

	li $v1, 1		# return val = 1
	checkDL_return:
		jr $ra

printBoard:
	li $v0, 4		# syscode 4 (print string)
	la $a0, topBord		# arg0 = topBord
	syscall

	la $a0, filler		# arg0 = filler
	syscall

	li $t1, 9		# cols = 9
	li $t2, 6		# rows = 6
	li $t3, 0		# i = 0
	mul $t5, $s3, $t1	# 4 * 9 (for i * 9)
	printB_for1:
		bge $t3, $t2, printB_endfor1	# if i >= rows
		li $t4, 0			# j = 0

		printB_for2:
			bge $t4, $t1, printB_endfor2	# if j >= cols
			mul $t6, $t3, $t5		# temp1 = i * 36
			mul $t7, $t4, $s3		# temp2 = j * 4
			add $t6, $t6, $t7		# temp1 = temp1 + temp2
			la $a0, board			# arg0 = board
			add $t6, $t6, $a0		# temp1 = temp1 + board
			
			li $v0, 11		# syscode 11 (print char)
			lw $a0, 0($t6)		# arg0 = board[(i*9)+j]
			syscall

			li $a0, 32		# ascii 32 (space)
			syscall
			
			addi $t4, $t4, 1	# j++
			j printB_for2
		printB_endfor2:
		
		li $v0, 4		# syscode 4 (print string)
		la $a0, newline		# arg0 = "\n"
		syscall

		addi $t3, $t3, 1	# i++
		j printB_for1
	printB_endfor1:

	li $v0, 4		# syscode 4 (print string)
	la $a0, filler		# arg0 = filler
	syscall

	jr $ra

playerTurn:
	move $t9, $ra		# stack_temp = return address
	jal push		# push stack_temp

	li $t2, 0		# col = 0

	playerT_while1:
		li $v0, 4	# syscode 4 (print string)
		la $a0, pPrompt	# arg0 = pPrompt
		syscall

		li $v0, 1	# syscode 1 (print int)
		li $a0, 1	# arg0 = 1
		syscall

		li $v0, 4	# syscode 4 (print string)
		la $a0, dash	# arg0 = "-"
		syscall

		li $v0, 1	# syscode 1 (print int)
		li $a0, 7	# arg0 = 7
		syscall

		li $v0, 4	# syscode 4 (print string)
		la $a0, colon	# arg0 = ": "
		syscall

		li $v0, 5	# syscode 5 (read int)
		syscall
		move $t2, $v0	# col = input

		li $t3, 8	# temp1 = 8

		playerT_if1:
			ble $t2, $zero, playerT_else1	# if col <= 0
			bge $t2, $t3, playerT_else1	# if col >= 8
			
			la $a0, board
			mul $t3, $t2, $s3		# temp1 = col * 4
			add $t3, $t3, $a0		# temp1 = temp1 + board
			lw $t3, 0($t3)			# temp1 = board[col]
			li $t4, 46			# temp2 = 46
			beq $t3, $t4, playerT_endwhile1	# if board[col] == 46
			
			li $v0, 4		# syscode 4 (print string)
			la $a0, cFull_p1	# arg0 = cFull_p1
			syscall
		
			li $v0, 1		# syscode 1 (print int)
			move $a0, $t2		# arg0 = col
			syscall

			li $v0, 4		# syscode 4 (print string)
			la $a0, cFull_p2	# arg0 = cFull_p2
			syscall

			j playerT_if_el1
		playerT_else1:
			li $v0, 4		# syscode 4 (print string)
			la $a0, inInput		# arg0 = inInput
			syscall
		playerT_if_el1:
		
		j playerT_while1
	playerT_endwhile1:
	
	la $a0, board
	move $a1, $t2		# arg1 = col
	li $a2, 1		# arg2 = 1
	jal updateBoard		# updateBoard(board, col, 1)

	jal pop			# stack_temp = return address
	jr $t9

computerTurn:
	move $t9, $ra		# stack_temp = return address
	jal push		# push stack_temp

	li $t2, 0		# col = 0

	computerT_while1:
		li $a0, 1		# arg0 = 1
		li $a1, 7		# arg1 = 7
		jal get_random_in_range	# get_random_in_range(1, 7)
		move $t2, $v1		# col = get_random_in_range(1, 7)

		la $a0, board		# arg0 = board (address)
		mul $t3, $t2, $s3	# temp1 = col * 4
		add $t3, $t3, $a0	# temp1 = temp1 + board
		lw $t3, 0($t3)		# temp1 = board[col]

		li $t4, 46				# temp2 = 46
		beq $t3, $t4, computerT_endwhile1	# if board[col] == 46
	
		j computerT_while1
	computerT_endwhile1:

	li $v0, 4		# syscode 4 (print string)
	la $a0, cPrompt		# arg0 = cPrompt
	syscall

	li $v0, 1		# syscode 1 (print int)
	move $a0, $t2		# arg0 = col
	syscall

	li $v0, 4 		# syscode 4 (print string)
	la $a0, newline		# arg0 = newline
	syscall

	la $a0, board		# arg0 = board (address)
	move $a1, $t2		# arg1 = col
	li $a2, 0		# arg2 = 0
	jal updateBoard		# updateBoard(board, col, 0)

	jal pop			# stack_temp = return address
	jr $t9

updateBoard:
	move $t2, $a1		# index = col
	li $t3, 0		# letter = 0
	li $t8, 1		# temp4 = 1

	updateB_if1:
		bne $a2, $t8, updateB_else1	# if player != 1
	
		li $t3, 72		# letter = 72
		j updateB_if_el1	# end ifelse
	updateB_else1:
		li $t3, 67		# letter = 67
	updateB_if_el1:

	updateB_while1:
		mul $t4, $t2, $s3		# temp1 = index * 4
		add $t5, $t4, $a0		# temp2 = temp1 + board
		addi $t5, $t5, 36		# temp2 = temp2 + 36
		lw $t5, 0($t5)			# temp2 = board[index+9]
		li $t6, 46			# temp3 = 46
		bne $t5, $t6, updateB_endwhile1	# if board[index+9] != 46
		bge $t2, $t6, updateB_endwhile1	# if index >= 46

		addi $t2, $t2, 9		# index += 9
		j updateB_while1
	updateB_endwhile1:

	add $t4, $t4, $a0	# temp1 = temp1 + board
	sw $t3, 0($t4)		# board[index] = letter
	jr $ra

get_random_in_range:
	move $t9, $ra		# stack_temp = $ra
	jal push		# push stack

	sub $t4, $a1, $a0	# range = high - low
	addi $t4, $t4, 1	# range++
	jal get_random		# result = get_random()

	divu $v1, $t4		# result / range
	mfhi $t4		# temp = remainder
	add $v1, $t4, $a0	# val = temp + low

	jal pop			# pop stack
	jr $t9

get_random:
	li $t5, 36969		# temp1 = 36969
	andi $t6, $s1, 65535	# temp2 = (m_z & 65535)
	mul $t6, $t5, $t6	# temp2 = temp1 * temp2
	srl $t7, $s1, 16	# temp3 = (m_z >> 16)
	addu $s1, $t6, $t7	# m_z = temp2 + temp3

	li $t5, 18000		# temp1 = 18000
	andi $t6, $s0, 65535	# temp2 = (m_w & 65535)
	mul $t6, $t5, $t6	# temp2 = temp1 * temp2
	srl $t7, $s0, 16	# temp3 = (m_w >> 16)
	addu $s0, $t6, $t7	# m_w = temp2 + temp3

	sll $t8, $s1, 16	# m_z = (m_z << 16)
	addu $v1, $t8, $s0	# result = m_z + m_w
	jr $ra

push:
	addi $sp, $sp, -4	# stack -= 4
	sw $t9, 0($sp)		# stack = stack_temp
	jr $ra

pop:
	lw $t9, 0($sp)		# stack_temp = stack
	addi $sp, $sp, 4	# stack += 4
	jr $ra

.data

m_w:	.word 0		# m_w = 0
m_z:	.word 0		# m_z = 0
size:	.word 54	# size = 54
board:	.space 216	# board[54]

intro_p1: .asciiz "\nWelcome to Connect Four, Five-in-a-Row variant.\nVersion "
period:   .asciiz "."
newline:  .asciiz "\n"
intro_p2: .asciiz "Implemented by Kevin Huynh\n\nEnter two positive numbers to initialize the random number generator."
number:	  .asciiz "\nNumber "
colon:    .asciiz ": "
intro_p3: .asciiz "\nHuman player (H)\nComputer player (C)\nCoin toss... "
human:    .asciiz "HUMAN"
computer: .asciiz "COMPUTER"
intro_p4: .asciiz " goes first.\n"

topBord:  .asciiz "\n  1 2 3 4 5 6 7  \n"
filler:   .asciiz "-----------------\n"

win:	  .asciiz "\nCongratulations, Human Winner.\n"
lose:	  .asciiz "\nGame over, Computer Wins.\n"
tie:	  .asciiz "\nIt's a tie.\n"

pPrompt:  .asciiz "What column would you like to drop the token into? Enter "
dash:	  .asciiz "-"
cFull_p1: .asciiz "\nColumn "
cFull_p2: .asciiz " is full.\n"
inInput:  .asciiz "\nInvalid input.\n"

cPrompt:  .asciiz "Computer player selected column "
