#ifndef CONNECT_H
#define	CONNECT_H

int* setupBoard();
int gameLoop(int *board, int order);
int printIntro();
int checkWin(int *board);
int checkTie(int *board);
int checkHorizontal(int *board, int index);
int checkDiagonalRight(int *board, int index);
int checkVertical(int *board, int index);
int checkDiagonalLeft(int *board, int index);
void printBoard(int *board);
void playerTurn(int *board);
void computerTurn(int *board);
void updateBoard(int *board, int col, int player);
void freeBoard(int *board);
int get_random_in_range(int low, int high);
uint32_t get_random();

#endif
