#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "connect.h"

uint32_t m_w;
uint32_t m_z;
int size = 54;

int main() {
	int *board = setupBoard();
	int order = printIntro();
	gameLoop(board, order);
	return 1;
}

int printIntro() {
	printf("\nWelcome to Connect Four, Five-in-a-Row variant!\n");
	printf("Version 1.0\n");
	printf("Implement by Kevin Huynh\n");
	printf("\nEnter two positive numbers to initialize the random number generator.");
	printf("\nNumber 1: ");
	scanf("%5i", &m_w);
	printf("\nNumber 2: ");
	scanf("%5i", &m_z);
	printf("\nHuman player (H)");
	printf("\nComputer player (C)");
	printf("\nCoin toss... ");
	int rand = get_random_in_range(0, 1);
	char *firstP;
	if (rand)
		firstP = "HUMAN";
	else
		firstP = "COMPUTER";
	printf("%s goes first.\n", firstP);
	return rand;
}

int* setupBoard() {
	int letter = 67;
	int *board = (int*)malloc(sizeof(int)*size);
	for (int i = 0; i < size; i++) {
		if (i % 9 == 0)
			board[i] = letter;
		else if (i % 9 == 8) {
			board[i] = letter;
			if (letter == 67)
				letter = 72;
			else
				letter = 67;
		}
		else
			board[i] = 46;
	}

	return board;
}

int gameLoop(int *board, int order) {
	int finished = 0;

	while (finished == 0) {
		printBoard(board);
		if (order) {
			playerTurn(board);
			finished = checkWin(board);
			if (finished)
				break;
			computerTurn(board);
		}
		else {
			computerTurn(board);
			finished = checkWin(board);
			if (finished)
				break;
			playerTurn(board);
		}
		finished = checkWin(board);
	}

	printBoard(board);

	if (finished == 72)
		printf("\nCongratulations, Human Winner!\n");
	else if (finished == 67)
		printf("\nGame over, Computer Wins!\n");
	else
		printf("\nIt's a tie.\n");

	return 1;
}

int checkWin(int *board) {
	for (int i = 0; i < size; i++) {
		if (board[i] == 46) {}
		else if (i > 17) {
			if (board[i] == board[i+1]) {
				if (checkHorizontal(board, i))
					return board[i];
			}
		}
		else {
			if (board[i] == board[i+9]) {
				if (checkVertical(board, i))
					return board[i];
			}
			if (i % 9 <= 4) {
				if (board[i] == board[i+1]) {
					if (checkHorizontal(board, i))
						return board[i];
				}
				if (board[i] == board[i+10]) {
					if (checkDiagonalRight(board, i))
						return board[i];
				}
			}
			if (i % 9 >= 4) {
				if (board[i] == board[i+8]) {
					if (checkDiagonalLeft(board, i))
						return board[i];
				}
			}
		}	
	}
	if (checkTie(board))
		return 2;
	return 0;
}

int checkTie(int *board) {
	for (int i = 1; i < 7; i++) {
		if (board[i] == 46)
			return 0;
	}
	return 1;
}

int checkHorizontal(int *board, int index) {
	for (int j = 2; j < 5; j++) {
		if (board[index] != board[index+j])
			return 0;
	}
	return 1;
}

int checkDiagonalRight(int *board, int index) {
	for (int j = 2; j < 5; j++) {
		if (board[index] != board[index+(j*10)])
			return 0;
	}
	return 1;
}

int checkVertical(int *board, int index) {
	for (int j = 2; j < 5; j++) {
		if (board[index] != board[index+(j*9)])
			return 0;
	}
	return 1;
}

int checkDiagonalLeft(int *board, int index) {
	for (int j = 2; j < 5; j++) {
		if (board[index] != board[index+(j*8)])
			return 0;
	}
	return 1;
}

void printBoard(int *board) {
	int cols = 9;
	int rows = 6;
	char *topBord = "\n  1 2 3 4 5 6 7  \n";
	char *filler = "-----------------\n";
	printf("%s", topBord);
	printf("%s", filler);
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			printf("%c ", board[(i*9)+j]);
		}
		printf("\n");
	}
	printf("%s", filler);
}

void playerTurn(int *board) {
	int col = 0;
	char *str = "What column would you like to drop token in to? Enter 1-7: ";
	do {
		printf("%s", str);
		scanf("%1i", &col);
		if (col > 0 && col < 8) {
			if (board[col] == 46)
				break;
			else
				printf("\nColumn %i is full.\n", col);
		}
		else
			printf("\nInvalid input.\n");
	} while (1);
	updateBoard(board, col, 1);
}

void computerTurn(int *board) {
	int col = 0;
	do {
		col = get_random_in_range(1, 7);
		if (board[col] == 46)
			break;
	} while(1);
	char *compPrompt = "Computer player selected column";
	printf("%s %i\n", compPrompt, col);
	updateBoard(board, col, 0);
}

void updateBoard(int *board, int col, int player) {
	int index = col;
	int letter = 0;
	if (player)
		letter = 72;
	else
		letter = 67;
	
	while (board[index+9] == 46 && index < 45)
		index+=9;
	
	board[index] = letter;
}

void freeBoard(int *board) {
	free(board);
}

int get_random_in_range(int low, int high) {
	uint32_t range = (high - low) + 1;
	uint32_t randNum = get_random();
	int rand = (int) ((randNum % range) + low);
	return rand;
}

uint32_t get_random() {
	m_z = 36969 * (m_z & 65535) + (m_z >> 16);
	m_w = 18000 * (m_w & 65535) + (m_w >> 16);
	return (m_z << 16) + m_w;
}
